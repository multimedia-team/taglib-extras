/***************************************************************************
      copyright            : (C) 2005 by Paul Cifarelli
      email                : paulc2@optonline.net

      copyright            : (C) 2005 by Lukas Lalinsky
      email                : lalinsky@gmail.com
                             (portions)
 ***************************************************************************/

/***************************************************************************
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

/***************************************************************************
 *   This library is free software; you can redistribute it and/or modify  *
 *   it  under the terms of the GNU Lesser General Public License version  *
 *   2 or higher as published by the Free Software Foundation.             *
 *                                                                         *
 *   This library is distributed in the hope that it will be useful, but   *
 *   WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
 *   Lesser General Public License for more details.                       *
 *                                                                         *
 *   You should have received a copy of the GNU Lesser General Public      *
 *   License along with this library; if not, write to the Free Software   *
 *   Foundation, Inc., 51 Franklin St, 5th fl, Boston, MA 02110-1301,      *
 *   USA, or check http://www.fsf.org/about/contact.html                   *
 ***************************************************************************/
#ifndef _TAGLIB_REALMEDIAFILE_H_
#define _TAGLIB_REALMEDIAFILE_H_

#include <taglib-extras_export.h>

#include <tfile_helper.h>
#include <audioproperties.h>
#include <tag.h>

#include <iostream>

namespace TagLibExtras {

   namespace RealMedia {
      class RealMediaFF;
      class TAGLIB_EXTRAS_EXPORT Tag : public TagLib::Tag
      {
      public:
         explicit Tag(RealMediaFF *rmff, bool allocnew = false);
         virtual   ~Tag ();
         virtual TagLib::String   title () const;
         virtual TagLib::String   artist () const;
         virtual TagLib::String   album () const;
         virtual TagLib::String   comment () const;
         virtual TagLib::String   genre () const;
         virtual TagLib::uint   year () const;
         virtual TagLib::uint   track () const;
         virtual void   setTitle (const TagLib::String &s);
         virtual void   setArtist (const TagLib::String &s);
         virtual void   setAlbum (const TagLib::String &s);
         virtual void   setComment (const TagLib::String &s);
         virtual void   setGenre (const TagLib::String &s);
         virtual void   setYear (TagLib::uint i);
         virtual void   setTrack (TagLib::uint i);

         bool isEmpty() const;
         void duplicate(const Tag *source, Tag *target, bool overwrite);

      private:
         Tag();
         RealMediaFF *m_rmff;
         bool m_owner;
      };


      class TAGLIB_EXTRAS_EXPORT Properties : public TagLib::AudioProperties
      {
      public:
         Properties(RealMediaFF *rmff) : TagLib::AudioProperties(Average), m_rmff(rmff) {}
         virtual ~Properties() {}  // you don't own rmff
         virtual int length () const;
         virtual int bitrate () const;
         virtual int sampleRate () const;
         virtual int channels () const;

      private:
         Properties();
         RealMediaFF *m_rmff;
      };

      class TAGLIB_EXTRAS_EXPORT File : public TagLib::File
      {
      public:

         explicit File(TagLib::FileName file, bool readProperties = true, Properties::ReadStyle propertiesStyle = Properties::Average);

         virtual ~File();

         /*
          * Returns the TagLib::Tag for this file.
          */
         virtual TagLib::Tag *tag() const;

         /*
          * Returns the RealMedia::RealMediaTag for this file.
          */
         virtual Tag *RealMediaTag() const;

         /*
          * Returns the RealMedia::Properties for this file.
          */
         virtual Properties *audioProperties() const;


         /*
          * Save the file.
          *
          * This returns true if the save was successful.
          */
         virtual bool save() { return false; } // for now

      private:

         RealMediaFF *m_rmfile;
         Tag         *m_tag;
         Properties  *m_props;
      };

   }

}

#endif

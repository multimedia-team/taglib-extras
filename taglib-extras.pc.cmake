prefix=${CMAKE_INSTALL_PREFIX}
exec_prefix=${CMAKE_INSTALL_PREFIX}
libdir=${LIB_INSTALL_DIR}
includedir=${INCLUDE_INSTALL_DIR}

Name: TagLib Extras
Description: Unofficial TagLib file type plugins maintained by the Amarok project
Requires:
Version: ${TAGLIB-EXTRAS_LIB_MAJOR_VERSION}.${TAGLIB-EXTRAS_LIB_MINOR_VERSION}.${TAGLIB-EXTRAS_LIB_PATCH_VERSION}
Libs: -L${LIB_INSTALL_DIR} -ltag-extras
Cflags: -I${INCLUDE_INSTALL_DIR}/taglib-extras
